# coding: utf-8

# In[13]:


# import pandas data frame this module helps in reading the csv into data frame 
import pandas as pd

# In[2]:


# Read the Training data, prsent in the csv format,
# Note: test.csv contains the customer review and there review sentiment , do not confuse with the file name 
dataset = pd.read_csv(filepath_or_buffer="D:/sentimental_analysis/code1/test.csv", sep='&;&;')

# In[3]:


# shows the top 5 content  of the csv file 
dataset.head()

# In[4]:


#  brief information on the contents precent in the csv file 
dataset.info()

# In[5]:


# brief description of the content present in the data frame named dataset 
dataset.describe()

# In[6]:


# checking for any None present in the data 
dataset.isnull().sum()

# In[7]:


# breaking dataset into the training dataset and Test dataset 

from sklearn.model_selection import train_test_split

train_review, test_review, train_sentiment, test_sentiment = train_test_split(
    dataset.review, dataset.sentiment, test_size=0.25, random_state=None)

# In[8]:


# Tokenizing text with scikit-learn¶
# Text preprocessing, tokenizing and filtering of stopwords are all included in CountVectorizer, 
# which builds a dictionary of features and transforms documents to feature vectors:

from sklearn.feature_extraction.text import CountVectorizer

count_vect = CountVectorizer()
train_review_counts = count_vect.fit_transform(train_review)
train_review_counts.shape

# In[12]:


train_review_counts.data

# In[105]:


# CountVectorizer supports counts of N-grams of words or consecutive characters. 
# Once fitted, the vectorizer has built a dictionary of feature indices:

# count_vect.vocabulary_
print(count_vect.vocabulary_.get(u'algorithm'))

# In[106]:


# downscaling From occurrences to frequencies

# To avoid these potential discrepancies it suffices to divide the number of occurrences of each word in a document 
# by the total number of words in the document: these new features are called tf for Term Frequencies.

# Another refinement on top of tf is to downscale weights for words that occur in many documents in the corpus and 
# are therefore less informative than those that occur only in a smaller portion of the corpus.

# This downscaling is called tf–idf for “Term Frequency times Inverse Document Frequency”.


tfidf_transformer = TfidfTransformer()
train_review_tfidf = tfidf_transformer.fit_transform(train_review_counts)
train_review_tfidf.shape

# In[107]:


# Training a classifier¶
# naïve Bayes classifier,

from sklearn.naive_bayes import MultinomialNB

clf = MultinomialNB().fit(train_review_tfidf, train_sentiment)

# In[109]:


# Building a pipeline (optional)
# In order to make the vectorizer => transformer => classifier easier to work with,

from sklearn.pipeline import Pipeline

text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])

# In[110]:


# We can now train the model with a single command:
text_clf.fit(train_review, train_sentiment)

# In[113]:


# tokenize the test reviews 
test_review_counts = count_vect.transform(test_review)
# downscaling From occurrences to frequencies  for test_review_counts 
test_review_tfidf = tfidf_transformer.transform(test_review_counts)
# making prediction object
predicted = clf.predict(test_review_tfidf)

# In[114]:


# Evaluation of the performance on the test set
import numpy as np

np.mean(predicted == test_sentiment)

# In[116]:


# plugging a different classifier object into our pipeline:

# linear support vector machine (SVM), 
# which is widely regarded  for text classification algorithms 

from sklearn.linear_model import SGDClassifier

text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                           alpha=1e-3, random_state=42,
                                           max_iter=5, tol=None)),
                     ])
text_clf.fit(train_review, train_sentiment)
predicted = text_clf.predict(test_review)
np.mean(predicted == test_sentiment)

# In[117]:


# detailed performance analysis of the results:
from sklearn import metrics

print(metrics.classification_report(test_sentiment, predicted))

# In[118]:


# confusion matrix 
metrics.confusion_matrix(test_sentiment, predicted)

# In[125]:


# Parameter tuning using grid search¶
#  more cpu consumption 
from sklearn.model_selection import GridSearchCV

parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
              'tfidf__use_idf': (True, False),
              'clf__alpha': (1e-2, 1e-3),
              }

# In[126]:


gs_clf = GridSearchCV(text_clf, parameters, n_jobs=-1)

# In[127]:



# search on a smaller subset of the training data to speed up the computation:
gs_clf = gs_clf.fit(train_review[:1000], train_sentiment[:1000])

# In[128]:


# The result of calling fit on a GridSearchCV object is a classifier that we can use to predict:

gs_clf.predict(['God is love'])

# In[129]:



test_sentence = """Better than ever: I was a previous user of the Logitech trackball mouse, which I found comfortable but did not like that took up a lot of precious real estate on my desk. It also required frequent cleaning of the contacts; eventually, the mouse was erratic as i may have bent the contacts when I cleaned them. When I saw this optical version, I ordered one for home and two for work. Since I am a long-term user, I love the thumb action, but the colleague I ordered the second one for at work couldn't get used to the track ball at the side. I am delighted. This mouse does just what an optical mouse should do - move about the screen without physically moving the whole unit.I was pleased that it takes up only slightly more space than a regular optical mouse (though the slope of my old space hog was still more comfortable!) I 'd say go for this prouduct!"""
gs_clf.predict([test_sentence])

# In[131]:


# The object’s best_score_ and best_params_ attributes store the best mean score and 
# the parameters setting corresponding to that score:

gs_clf.best_score_

# In[132]:


for param_name in sorted(parameters.keys()):
    print("%s: %r" % (param_name, gs_clf.best_params_[param_name]))

# In[135]:


# A more detailed summary of the search is available at gs_clf.cv_results_.

gs_clf.cv_results_
